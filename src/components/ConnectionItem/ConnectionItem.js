import React from "react";
import {
    Checkbox
} from '@atlaskit/checkbox';
import './ConnectionItem.css'
import Button from '@atlaskit/button';
import { RadioGroup } from '@atlaskit/radio';

const ConnectionItem = props => {

    const radioValues = [
        { name: 'color', value: 'blue', label: 'Blue' },
        { name: 'color', value: 'red', label: 'Red' },
        { name: 'color', value: 'purple', label: 'Purple' },
    ];

    return(


        <div className="ConnectionItem">
            <h3>{props.item.name}</h3>
            <Checkbox
                label="Basic checkbox"
            />
            <Checkbox
                label="Basic checkbox"
            />
            <hr/>
            <RadioGroup
                label="Pick a color"
                defaultValue={radioValues[2].value}
                options={radioValues}
            />
            <Button
                appearance="danger"
            >Danger</Button>
        </div>
    )
}

export default ConnectionItem