import React from 'react';
import {
    MenuGroup,
    Section,
    HeadingItem,
    LinkItem
} from '@atlaskit/menu';
import VidConnectionCircleIcon from '@atlaskit/icon/glyph/vid-connection-circle';



const Menu = (props) => {
    let connections = null;
    connections = props.connectionList.map((connection, index) => {
        return (
            <LinkItem
                style={{textDecoration: 'none'}}
                description={`Description for ${connection.name}`}
                elemBefore={<VidConnectionCircleIcon/>}
                //LINK HERE
                href={connection.name}
            >
                {connection.name}
            </LinkItem>
        )
    })

    return(
        <MenuGroup>
            <Section>
                <HeadingItem>Connections</HeadingItem>
                {connections}
            </Section>
        </MenuGroup>
    )
}

export default Menu