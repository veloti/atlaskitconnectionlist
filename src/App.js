import React, { Component } from 'react';
import './App.css';
import {Route} from 'react-router-dom';
import ConnectionItem from "./components/ConnectionItem/ConnectionItem";
import Menu from './components/Menu/Menu';
import EmptyState from '@atlaskit/empty-state';

class App extends Component {
    state = {
      connections: [
          {name: 'connection1'},
          {name: 'connection2'},
          {name: 'connection3'},
          {name: 'connection4'},
          {name: "connection5"}
      ],
    }

    render() {
        let routes = null;

        routes = this.state.connections.map((connection, index) => {
            return (
                <Route
                    key={index}
                    path={`/${connection.name}`}
                    render={() => <ConnectionItem item={connection}/>}
                />
            )
        })

        return(
            <div className="App">
                {
                    this.state.connections.length
                    ?
                        <>
                            {routes}
                            <Route path="/" exact render={() =>
                                <Menu
                                    connectionList={this.state.connections}
                                />}
                            />
                        </>


                    : <EmptyState/>
                }
            </div>
        )
    }
}

export default App;
